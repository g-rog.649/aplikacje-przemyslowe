package com.tje.users;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.tje.users.User.UserType;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

@Controller
public class UsersController {

    @GetMapping("/")
    public String home(Model model) throws ParseException {
        String startDateString = "20/05/2007 07:32";
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date startDate = df.parse(startDateString);
        User user = new User(2, "Artur", 34, User.UserType.ADMIN, startDate);
        model.addAttribute("user", user);
        return "home";
    }

    @GetMapping("/list")
    public String list(Model model) throws ParseException {
        ArrayList<User> users = new ArrayList<User>();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        
        users.add(new User(1, "Jillian", 26, UserType.ADMIN, df.parse("2022-02-17 11:01")));
        users.add(new User(2, "Mohandas", 22, UserType.GUEST, df.parse("2023-06-27 10:47")));
        users.add(new User(3, "Philly", 19, UserType.REGISTERED, df.parse("2022-06-21 10:12")));
        users.add(new User(4, "Kirsteni", 61, UserType.REGISTERED, df.parse("2022-08-22 16:13")));
        users.add(new User(5, "Cosetta", 34, UserType.ADMIN, df.parse("2023-03-11 12:35")));
        users.add(new User(6, "Hedvig", 71, UserType.GUEST, df.parse("2023-06-20 13:47")));
        users.add(new User(7, "Lorna", 28, UserType.REGISTERED, df.parse("2023-07-22 15:18")));
        users.add(new User(8, "Arty", 55, UserType.GUEST, df.parse("2022-06-14 14:25")));

        model.addAttribute("users", users);
        return "list";
    }
}
