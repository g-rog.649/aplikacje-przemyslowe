package com.example.validForm.validation;


import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class PostCodeValidator implements ConstraintValidator<PostCode, String> {

    private Pattern postcodePattern = Pattern.compile("\\d{2}-\\d{3}");
    
    public void initialize(PostCode constraint) {}

    @Override
    public boolean isValid(String postCode, ConstraintValidatorContext constraintValidatorContext) {
        return postcodePattern.matcher(postCode).matches();
    }

}
