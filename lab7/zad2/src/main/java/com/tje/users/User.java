package com.tje.users;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class User {
    private int id;
    
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be at least two characters long")
    private String name;
    
    @NotNull(message = "Age is required")
    @Min(value = 1, message = "Age must be greater than zero")
    private int age;
    
    @NotNull(message = "User type is required")
    private UserType userType;

    @NotNull(message = "Registration date is required")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'hh:mm")
    private Date registrationDate;

    public User() {}
    
    public User(int id, String name, int age, UserType userType, Date registrationDate) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.userType = userType;
        this.registrationDate = registrationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public long getRegistrationDaysAgo() {
        return Duration.between(
            getRegistrationDate().toInstant(), 
            Instant.now()
        ).toDays();
    }

    public static enum UserType {
        GUEST, REGISTERED, ADMIN
    }
}
