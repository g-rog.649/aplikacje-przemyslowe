package com.example.validForm;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class Person {
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be start at least two characters")
    private String name;

    @NotNull(message = "Age is required")
    @Min(value = 0, message = "Age must be at least zero")
    private int age;

    @NotNull(message = "Post code is required")
    @Pattern(regexp = "\\d\\d[-]\\d\\d\\d", message = "Wrong post code format")
    private String postCode;

    @NotNull(message = "Salary is required")
    @Min(2000)
    @Max(3000)
    private int salary;

    @AssertTrue(message = "Terms of service not accepted")
    private boolean acceptTerms;
}
