#!/bin/bash
echo "POST:"
person_id=$(curl -s localhost:9393/people -X POST -H "Content-Type: application/json" -d '{"firstName":"Jan","lastName":"Kowalski","email":"jan.kowalski@gmail.com","companyName":"UG, Inc."}')
echo $person_id

echo "GET:"
get_person=$(curl -s localhost:9393/people/$person_id)
echo $get_person | jq

echo "PUT:"
person_changed=$(curl -s localhost:9393/people/$person_id -X PUT -H "Content-Type: application/json" -d '{"firstName":"Janusz","email":"janusz.kowalski@gmail.com"}')
echo $person_changed | jq

echo "DELETE:"
deleted=$(curl -s localhost:9393/people/$person_id -X DELETE)
echo $deleted

echo "GET:"
get_person=$(curl -s localhost:9393/people/$person_id)
echo $get_person | jq

