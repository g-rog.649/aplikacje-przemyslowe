package pl.edu.ug.tent.springintro.restservice;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.edu.ug.tent.springintro.domain.Person;

@RestController
@RequestMapping("/people")
public class PersonController {
  public static Map<String, Person> people = new HashMap<String, Person>();
  private Random rnd = new Random();

  @GetMapping("")
  public Map<String, Person> getPeople() {
    return people;
  }

  @GetMapping("/{id}")
  public Person getPerson(@PathVariable String id) {
    return people.get(id);
  }

  @PostMapping("")
  public int addPerson(@RequestBody Person person) {
    int id = Math.abs(rnd.nextInt());
    people.put(String.valueOf(id), person);
    return id;
  }

  @PutMapping("/{id}")
  public ResponseEntity<Person> updatePerson(
      @RequestBody Person person,
      @PathVariable String id
  ) {
    if (people.containsKey(id)) {
      Person personGet = people.get(id);
      personGet.update(
        person.getFirstName(),
        person.getLastName(),
        person.getEmail(),
        person.getCompanyName()
      );
      return ResponseEntity.ok(personGet);
    } else {
      return ResponseEntity.notFound().build();
    }
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Person> deletePerson(@PathVariable String id) {
    people.remove(id);
    return ResponseEntity.ok().build();
  }
}
