package pl.edu.ug.tent.springintro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.ug.tent.springintro.domain.Person;

@Service
public class PersonService {
  @Autowired
  @Qualifier("president")
  Person president;

  Person getPresident() {
    return president;
  }

  @Autowired
  @Qualifier("vicepresident")
  Person vicePresident;

  Person getVicePresident() {
    return vicePresident;
  }

  @Autowired
  @Qualifier("secretary")
  Person secretary;

  Person getSecretary() {
    return secretary;
  }
}
