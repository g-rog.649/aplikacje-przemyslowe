package com.example.validForm.validation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PostCodeValidator.class)
public @interface PostCode {
 
    String message() default "Invalid post code";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
 
}
