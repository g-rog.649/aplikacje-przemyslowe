package pl.edu.ug.tent.springintro;

import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

import pl.edu.ug.tent.springintro.domain.Person;
import pl.edu.ug.tent.springintro.restservice.PersonController;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class SpringintroApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringintroApplication.class, args);

		Map<String, Person> people = context.getBeansOfType(Person.class);
		PersonController.people.putAll(people);
	}

}
