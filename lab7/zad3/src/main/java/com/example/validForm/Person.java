package com.example.validForm;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.example.validForm.validation.PostCode;
import com.example.validForm.validation.Salary;

import lombok.Data;

@Data
public class Person {
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be at least two characters long")
    private String name;

    @NotNull(message = "Age is required")
    @Min(value = 0, message = "Age should be at least zero")
    private int age;

    @NotNull(message = "Post code is required")
    @PostCode
    private String postCode;

    @NotNull(message = "Salary is required")
    @Salary
    private int salary;

    @AssertTrue(message = "Terms of service not accepted")
    private boolean acceptTerms;
}
