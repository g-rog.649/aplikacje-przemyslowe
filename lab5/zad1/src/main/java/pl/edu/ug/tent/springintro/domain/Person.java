package pl.edu.ug.tent.springintro.domain;

public class Person {
  private String firstName;
  private String lastName;
  private String email;
  private String companyName;

  public Person(String firstName, String lastName, String email, String companyName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.companyName = companyName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public boolean update(String firstName, String lastName, String email, String companyName) {
    boolean updated = false;
    if (firstName != null) {
      setFirstName(firstName);
      updated = true;
    }
    if (lastName != null) {
      setLastName(lastName);
      updated = true;
    }
    if (email != null) {
      setEmail(email);
      updated = true;
    }
    if (companyName != null) {
      setCompanyName(companyName);
      updated = true;
    }

    return updated;
  }

  @Override
  public String toString() {
    return String.format(
      "Person{firstName=%s, lastName=%s, email=%s, companyName=%s}",
      firstName, lastName, email, companyName
    );
  }
}
